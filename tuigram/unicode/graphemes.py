from typing import Literal
from tuigram.unicode import tables as t
from typing import NamedTuple


class DecodeResult[T](NamedTuple):
    value: T
    text: str


type Decoded[T] = DecodeResult[T] | None


def binary_search(c: int, table: tuple[int, ...], length: int, stride: int):
    n = length
    t = 0
    while n > 1:
        m = n // 2
        p = t + m * stride
        p = int(p)
        if c >= table[p]:
            t = p
            n = n - m
        else:
            n = m

    if n != 0 and c >= table[t]:
        return t

    return -1


def is_emoji_extended_pictographic(c: int) -> bool:
    p = binary_search(
        c,
        t.emoji_extended_pictographic_ranges,
        len(t.emoji_extended_pictographic_ranges) // 2,
        2,
    )
    if p >= 0 and (
        t.emoji_extended_pictographic_ranges[p]
        <= c
        <= t.emoji_extended_pictographic_ranges[p + 1]
    ):
        return True
    return False


class Char:
    def __init__(self, char) -> None:
        self.char = char

    def __repr__(self) -> str:
        return f"Char({self.char})"


class Emoji_Element:
    def __init__(
        self,
        char: str,
        el_type: Literal["char", "region", "mod", "zwj"],
        terminal_width: int,
    ) -> None:
        self.char = char
        self.el_type = el_type
        self.terminal_width = terminal_width

    def __repr__(self) -> str:
        return f"{self.el_type}({self.char}, {self.terminal_width})"


class Grapheme:
    def __init__(
        self,
        text,
        gf_type: Literal["emoji", "character"],
        terminal_width: int,
        chars: list,
    ) -> None:
        self.text = text
        self.chars = chars
        self.gf_type = gf_type
        self.terminal_width = terminal_width

    def __repr__(self) -> str:
        return (
            f"{self.gf_type}({self.text}, {self.terminal_width}, {self.chars})"
        )

    def __str__(self) -> str:
        return self.text


    def terminal_view(self) -> str:
        if self.gf_type == "character":
            return self.text

        s = ""
        for i, c in enumerate(self.chars):
            s += c.char
            if i == len(self.chars) - 1 and c.el_type == "mod":
                s += "\u200d "

        return s


def decode_emoji_character(text) -> Decoded[str]:
    if len(text) > 0 and is_emoji_extended_pictographic(ord(text[0])):
        return DecodeResult(text[0], text[1:])
    return None


def decode_zwj(text) -> Decoded[Emoji_Element]:
    if len(text) > 0 and text[0] == "\u200d":
        return DecodeResult(Emoji_Element("\u200d", "zwj", 0), text[1:])
    return None


def decode_emoji(text: str) -> Decoded[list[Emoji_Element]]:
    if zwj_element := decode_zwj_element(text):
        if zjw := decode_zwj(zwj_element.text):
            if next_zjw_element := decode_emoji(zjw.text):
                return DecodeResult(
                    [*zwj_element.value, zjw.value] + next_zjw_element.value,
                    next_zjw_element.text,
                )
            return DecodeResult([*zwj_element.value, zjw.value], zjw.text)
        return DecodeResult([*zwj_element.value], zwj_element.text)
    return None


def decode_emoji_modification(text) -> Decoded[Emoji_Element]:
    if len(text) > 0 and ord(text[0]) in range(0xFE00, 0xFE0F + 1):
        return DecodeResult(Emoji_Element(text[0], "mod", -1), text[1:])
    return None


def decode_flag_sequence(text) -> Decoded[list[Emoji_Element]]:
    regional_indicators = [
        Emoji_Element(x, "region", 1)
        for x in text[:2]
        if ord(x) in range(0x1F1E6, 0x1F1FF + 1)
    ]

    if len(regional_indicators) == 2:
        return DecodeResult(regional_indicators, text[2:])
    return None


def decode_zwj_element(text) -> Decoded[list[Emoji_Element]]:
    if flag := decode_flag_sequence(text):
        return flag

    if emoji_char := decode_emoji_character(text):
        emoji_element = Emoji_Element(emoji_char.value, "char", 2)

        if mod_res := decode_emoji_modification(emoji_char.text):
            return DecodeResult([emoji_element, mod_res.value], mod_res.text)
        return DecodeResult([emoji_element], emoji_char.text)

    return None


def decode_graphemes(text: str) -> list[Grapheme]:
    if len(text) <= 0:
        return []

    if emoji := decode_emoji(text):
        term_width = sum(e.terminal_width for e in emoji.value[:-1])

        # INFO: Emoji modification seem to have different widths weather they are
        #       in the middle of a grapheme or at the end
        if (el := emoji.value[-1]).el_type != "mod":
            term_width += el.terminal_width
        else:
            term_width += 0

        gr = Grapheme(
            "".join(e.char for e in emoji.value),
            "emoji",
            term_width,
            emoji.value,
        )
        return [gr] + decode_graphemes(emoji.text)

    return [Grapheme(text[0], "character", 1, [text[0]])] + decode_graphemes(
        text[1:]
    )


def gr_term_len(graphemes: list[Grapheme]) -> int:
    return sum(gr.terminal_width for gr in graphemes)


def text_term_len(text: str) -> int:
    return gr_term_len(decode_graphemes(text))
