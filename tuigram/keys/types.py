from typing import NamedTuple
from enum import Enum

class KeyType(Enum):

    NUL  = "<null>"                     # "ctrl+@"    # null, \0
    SOH  = "<strat-of-heading>"         # "ctrl+a"    # start of heading
    STX  = "<start-of-text>"            # "ctrl+b"    # start of text
    ETX  = "<break>"                    # "ctrl+c"    # break, ctrl+c
    EOT  = "<end-of-tranmission>"       # "ctrl+d"    # end of transmission
    ENQ  = "<enquiry>"                  # "ctrl+e"    # enquiry
    ACK  = "<acknowledge>"              # "ctrl+f"    # acknowledge
    BEL  = "<bell>"                     # "ctrl+g"    # bell, \a
    BS   = "<backspace>"                # "ctrl+h"    # backspace
    HT   = "<tab>"                      # "ctrl+i"    # horizontal tabulation, \t, or ctrl+i
    LF   = "<line-feed>"                # "ctrl+j"    # line feed, \n
    VT   = "<vertical-tabulation>"      # "ctrl+k"    # vertical tabulation \v
    FF   = "<formm-feed>"               # "ctrl+l"    # form feed \f
    CR   = "<enter>"                    # "enter"     # carriage return, \r, or ctrl+m
    SO   = "<shift-out>"                # "ctrl+n"    # shift out
    SI   = "<shift-in>"                 # "ctrl+o"    # shift in
    DLE  = "<data-link-escape>"         # "ctrl+p"    # data link escape
    DC1  = "<device-control-one>"       # "ctrl+q"    # device control one
    DC2  = "<device-control-two>"       # "ctrl+r"    # device control two
    DC3  = "<device-control-three>"     # "ctrl+s"    # device control three
    DC4  = "<device-control-four>"      # "ctrl+t"    # device control four
    NAK  = "<negative-acknowledge>"     # "ctrl+u"    # negative acknowledge
    SYN  = "<synchronous-idle>"         # "ctrl+v"    # synchronous idle
    ETB  = "<end-of-tranmission-block>" # "ctrl+w"    # end of transmission block
    CAN  = "<cancel>"                   # "ctrl+x"    # cancel
    EM   = "<end-of-medium>"            # "ctrl+y"    # end of medium
    SUB  = "<substitution>"             # "ctrl+z"    # substitution
    ESC  = "<esc>"                      # "ctrl+["    # escape, \e
    FS   = "<file-separator>"           # "ctrl+\\"   # file separator
    GS   = "<group-separator>"          # "ctrl+]"    # group separator
    RS   = "<record-separator>"         # "ctrl+^"    # record separator
    US   = "<unit-separator>"           # "ctrl+"    # unit separator
    DEL  = "<backspace>"                # "backspace" # delete. on most systems this is mapped to backspace, I hear
    Text = "[text]"

    A = "a"
    B = "b"
    C = "c"
    D = 'd'
    E = 'e'
    F = 'f'
    G = 'g'
    H = 'h'
    I = 'i'
    J = 'j'
    K = 'k'
    L = 'l'
    M = 'm'
    N = 'n'
    O = 'o'
    P = 'p'
    Q = 'q'
    R = 'r'
    S = 's'
    T = 't'
    U = 'u'
    V = 'v'
    W = 'w'
    X = 'x'
    Y = 'y'
    Z = 'z'

    SPACE                = ' '
    EXCLAMATION_MARK     = '!'
    QUOTATION_MARK       = '"'
    NUMBER_SIGN          = '#'
    DOLLAR_SIGN          = '$'
    PERCENT_SIGN         = '%'
    AMPERSAND            = '&'
    APOSTROPHE           = "'"
    LEFT_PARENTHESIS     = '('
    RIGHT_PARENTHESIS    = ')'
    ASTERISK             = '*'
    PLUS_SIGN            = '+'
    COMMA                = ','
    HYPHEN               = '-'
    PERIOD               = '.'
    SLASH                = '/'
    DIGIT_0              = '0'
    DIGIT_1              = '1'
    DIGIT_2              = '2'
    DIGIT_3              = '3'
    DIGIT_4              = '4'
    DIGIT_5              = '5'
    DIGIT_6              = '6'
    DIGIT_7              = '7'
    DIGIT_8              = '8'
    DIGIT_9              = '9'
    COLON                = ':'
    SEMICOLON            = ';'
    LESS_THAN            = '<'
    EQUALS_SIGN          = '='
    GREATER_THAN         = '>'
    QUESTION_MARK        = '?'
    AT_SIGN              = '@'
    LEFT_SQUARE_BRACKET  = '['
    BACKSLASH            = '\\'
    RIGHT_SQUARE_BRACKET = ']'
    CARET                = '^'
    UNDERSCORE           = '_'
    GRAVE_ACCENT         = '`'
    LEFT_CURLY_BRACE     = '{'
    VERTICAL_BAR         = '|'
    RIGHT_CURLY_BRACE    = '}'
    TILDE                = '~'

    UP    = '<arrow-up>'
    RIGHT = '<arrow-right>'
    DOWN  = '<arrow-down>'
    LEFT  = '<arrow-left>'

    HOME = 'home'
    END = 'end'

    DELETE = 'delete'

class ModKey(Enum):
    CONTROL = 0b001
    SHIFT   = 0b010
    ALT     = 0b100

class Key(NamedTuple):
    type: KeyType
    value: str
    mods: tuple[ModKey, ...]
    pasted: bool
