import runpy
import subprocess
import os
from random import choice
from pathlib import Path
from tempfile import NamedTemporaryFile

from tuigram.controllers import Controller, msg_handler, chat_handler
from tuigram.utils import suspend, get_mime, is_yes, is_no
from tuigram.tdlib import InputMessageContent
from tuigram.config import CONFIG_DIR

personal = runpy.run_path(os.path.join(CONFIG_DIR, "personal.py")) # File with personal constants


def p(calea: str):
    return Path(calea).expanduser()


PHONE = personal.get("PHONE")

MAILCAP = {
"image/webp": 'mpv --vo=tct {file_path}',

# media
"video/*": 'mpv {file_path}',
"audio/*": 'mpv --no-video {file_path}',
"image/*": 'nsxiv -a {file_path}',

# text
"text/html": 'link {file_path}',
"text/html": 'open -a Firefox {file_path}',
"text/plain": 'nvim {file_path}',

# fallback to vim
"text/*": 'nvim "{file_path}"',

# docs
"application/pdf": 'zathura {file_path}',
}



def notify(
    icon_path,
    title,
    subtitle,
    msg,
):
    subprocess.Popen(["dunstify", title, msg])


NOTIFY_FUNCTION = notify

DRAFTS_FILE = "/tmp/.drafts.json"

CHAT_FLAGS = {
    "online": "💡(online)",
    "pinned": "",
    "muted": "",
    "unread": "<>UNREAD!!",
    "unseen": "🙈",
    "secret": "🔒",
    "seen": "👀",
}

MSG_FLAGS = {
    "selected": "*----->",
    "forwarded": " :: {sender} >>",
    "new": "🥚",
    "unseen": "🙈",
    "edited": "E at {edit_date}",
    "pending": "󰮯...",
    "failed": "💩BRUH",
    "seen": "👀",
}

DOWNLOAD_DIR = "Downloads/TG/"

FILE_PICKER_CMD = "yazi --chooser-file {file_path}"
DIR_PICKER_CMD = "yazi --chooser-file {file_path}"
VIEW_PHOTOS_CMD = "mpv {file_path}"


EDITOR = "nvim"
LONG_MSG_CMD = "nvim {file_path}"


def gm(ctrl, *args) -> None:
    """Sends GM stiker to current chat"""
    chat_id = ctrl.model.chats.id_by_index(ctrl.model.current_chat)

    gm_path = p("~/Pics/stikers/frens/gm/") # A folder with stickers (webp format)
    gm_photo = choice(list(gm_path.iterdir()))

    ctrl.tg.send_doc(
        file_path=str(gm_photo),
        chat_id=chat_id,
        input_type=InputMessageContent.Document,
    )


def gn(ctrl: Controller, *args) -> None:
    """Sends GN stiker to current chat"""
    chat_id = ctrl.model.chats.id_by_index(ctrl.model.current_chat)
    if chat_id is None:
        return ctrl.present_error("No chat was selected")

    gn_path = p("~/Pics/stikers/frens/gn/")
    gn_photo = choice(list(gn_path.iterdir()))

    ctrl.tg.send_doc(
        file_path=str(gn_photo),
        chat_id=chat_id,
        input_type=InputMessageContent.Document,
    )


def gm_all(ctrl, *args):
    """Sends GM to all the frens"""
    chat_ids = personal.get("FRENS", []) # A list of your frineds' IDs

    gm_path = p("~/Pics/stikers/frens/gm/") # Path to a sticker folder
    gm_photo = choice(list(gm_path.iterdir()))
    info = ""

    for chat_id in chat_ids:
        ctrl.tg.send_doc(
            file_path=str(gm_photo),
            chat_id=chat_id,
            input_type=InputMessageContent.Document,
        )
        name = ctrl.model.users.get_user_label(chat_id)
        info += f"GM sent to {name}; "

    ctrl.present_info(info)


def gn_all(ctrl, *args):
    """Sends GN to all the frens"""
    chat_ids = personal.get("FRENS", []) # A list of your frineds' IDs

    gn_path = p("~/Pics/stikers/frens/gn/") # Path to a sticker folder
    gn_photo = choice(list(gn_path.iterdir()))

    info = ""
    for chat_id in chat_ids:
        ctrl.tg.send_doc(
            file_path=str(gn_photo),
            chat_id=chat_id,
            input_type=InputMessageContent.Document,
        )
        name = ctrl.model.users.get_user_label(chat_id)
        info += f"GN sent to {name}; "

    ctrl.present_info(info)

def send_stiker(ctrl: Controller, *arg) -> None:
    """Reply to a message with sticker"""
    chat_id = ctrl.model.chats.id_by_index(ctrl.model.current_chat)

    if chat_id is None:
        return ctrl.present_error("No chat selected")

    stiker_path = Path("~/Pics/stikers/").expanduser() # Path to your sticker packs. That are directories with webp images (or even gifs)

    if not stiker_path.exists():
        return ctrl.present_error("No stiker_folder found")

    try:
        with NamedTemporaryFile("w") as f, suspend(ctrl.view) as s:
            s.call(
                f"{FILE_PICKER_CMD.format(file_path=f.name)} {stiker_path}",
            )
            # s.call(f"ranger --choosedir={f.name} {stiker_path}",)
            with open(f.name) as f:
                dir_path = f.read().strip()
            ctrl.present_info(f"Directory path {dir_path}")
            cproc = s.call(f"nsxiv -t -o {dir_path}/*", capture_output=True)
            stiker_paths = cproc.stdout.decode("utf-8").strip().split("\n")

            ### HOW TO USE
            # /* nsxiv required */
            #
            # Chose a stiker folder with your file chooser
            # It will show all the stickers with nsxiv
            # Mark the chosen stickers 'type m' and quit nsxiv

            for stiker_path in stiker_paths:
                ctrl.tg.send_doc(
                    file_path=stiker_path,
                    chat_id=chat_id,
                    input_type=InputMessageContent.Document,
                )

            ctrl.present_info(
                "Stikers sent: "
                + ", ".join([path.split("/")[-1] for path in stiker_paths])
            )

    except FileNotFoundError:
        pass

def reply_stiker(ctrl: Controller, *arg) -> None:
    """Reply to a message with sticker"""
    chat_id = ctrl.model.chats.id_by_index(ctrl.model.current_chat)

    if chat_id is None:
        return ctrl.present_error("No chat selected")

    stiker_path = Path("~/Pics/stikers/").expanduser() # Path to your sticker packs. That are directories with webp images (or even gifs)

    if not stiker_path.exists():
        return ctrl.present_error("No stiker_folder found")

    try:
        with NamedTemporaryFile("w") as f, suspend(ctrl.view) as s:
            s.call(
                f"{FILE_PICKER_CMD.format(file_path=f.name)} {stiker_path}",
            )
            # s.call(f"ranger --choosedir={f.name} {stiker_path}",)
            with open(f.name) as f:
                dir_path = f.read().strip()
            ctrl.present_info(f"Directory path {dir_path}")
            cproc = s.call(f"nsxiv -t -o {dir_path}/*", capture_output=True)
            stiker_paths = cproc.stdout.decode("utf-8").strip().split("\n")

            for stiker_path in stiker_paths:
                ctrl.tg.send_doc(
                    file_path=stiker_path,
                    chat_id=chat_id,
                    input_type=InputMessageContent.Document,
                    reply_to_message_id=ctrl.model.current_msg_id,
                )

            ctrl.present_info(
                "Stikers sent: "
                + ", ".join([path.split("/")[-1] for path in stiker_paths])
            )

    except FileNotFoundError:
        pass




def save_file_in_folder(ctrl: Controller, *args) -> None:
    """Save chosen files to the current directory"""
    chat_id = ctrl.model.chats.id_by_index(ctrl.model.current_chat)
    if not chat_id:
        return

    msg_ids = ctrl.model.selected[chat_id]
    if not msg_ids:
        msg = ctrl.model.current_msg
        msg_ids = [msg["id"]]

    else:
        ctrl.discard_selected_msgs()

    if ctrl.model.copy_files(chat_id, msg_ids, str(Path().cwd())):
        ctrl.present_info(f"Copied files to {Path().cwd()}")


def send_multiple_files(ctrl: Controller, *args) -> None:
    """Call file picker and send selected files based on mimetype"""
    chat_id = ctrl.model.chats.id_by_index(ctrl.model.current_chat)
    file_path = None

    if not chat_id:
        return ctrl.present_error("No chat selected")
    try:
        with NamedTemporaryFile("w") as f, suspend(ctrl.view) as s:
            if FILE_PICKER_CMD is None:
                file_path = ctrl._recursive_fzf_file_chooser()
            else:
                s.call(FILE_PICKER_CMD.format(file_path=f.name))

                with open(f.name) as f:
                    file_path = f.read().strip().split()

    except FileNotFoundError:
        pass

    if file_path is None:
        ctrl.present_info("No files")
        return

    resp_up = ctrl.view.status.get_input(ctrl.view, f"upload files? [Y/n]")

    if resp_up is None or is_no(resp_up):
        return ctrl.present_info("Uploading cancelled")

    for file in file_path:
        mime_map = {
            "animation": InputMessageContent.Animation,
            "image": InputMessageContent.Photo,
            "audio": InputMessageContent.Audio,
            "video": InputMessageContent.Video,
        }

        mime = get_mime(file)

        msg_type = mime_map.get(mime, InputMessageContent.Document)
        ctrl.tg.send_doc(file, chat_id, msg_type)


CUSTOM_KEYBINDS = {
    "M": {"func": gm, "handler": msg_handler},                 # SHIFT + M : Sends random GM sticker to current chat
    "N": {"func": gn, "handler": msg_handler},                 # SHIFT + N : Sends random GN sticker to current chat
    "^A": {"func": gm_all, "handler": msg_handler},            # CTRL + A  : Sends random GM stiker to FRENs
    "^N": {"func": gn_all, "handler": msg_handler},            # CTRL + N  : Sends random GM stiker to FRENs
    "^S": {"func": send_stiker, "handler": msg_handler},       # CTRL + S  : Send sticker(s)
    "R^S": {"func": reply_stiker, "handler": msg_handler},     # R, CTRL + S (keychord) : Reply with sticker(s)
    "C": {"func": save_file_in_folder, "handler": msg_handler},# SHIFT + C : Copy selected files to current folder
    "B": {"func": send_multiple_files, "handler": msg_handler},# SHIFT + B : Send multiple files
}
